import ATAR_SCALED from "./js/atar-scaled.json";

// Storage Controller
const StorageCtrl = (function() {
  // Public methods
  return {
    storeItem: function(item) {
      let items;
      // Check if any items in ls
      if (localStorage.getItem("items") === null) {
        items = [];
        // Push new item
        items.push(item);
        // Set ls
        localStorage.setItem("items", JSON.stringify(items));
      } else {
        // Get what is already in ls
        items = JSON.parse(localStorage.getItem("items"));

        // Push new item
        items.push(item);

        // Re set ls
        localStorage.setItem("items", JSON.stringify(items));
      }
    },
    getItemsFromStorage: function() {
      let items;
      if (localStorage.getItem("items") === null) {
        items = [];
      } else {
        items = JSON.parse(localStorage.getItem("items"));
      }
      return items;
    },
    updateItemStorage: function(updatedItem) {
      let items = JSON.parse(localStorage.getItem("items"));

      items.forEach(function(item, index) {
        if (updatedItem.id === item.id) {
          items.splice(index, 1, updatedItem);
        }
      });
      localStorage.setItem("items", JSON.stringify(items));
    },
    deleteItemFromStorage: function(id) {
      let items = JSON.parse(localStorage.getItem("items"));

      items.forEach(function(item, index) {
        if (id === item.id) {
          items.splice(index, 1);
        }
      });
      localStorage.setItem("items", JSON.stringify(items));
    },
    clearItemsFromStorage: function() {
      localStorage.removeItem("items");
    }
  };
})();

// Item Controller
const ItemCtrl = (function() {
  // Item Constructor
  const Item = function(id, name, bonus, checked) {
    this.id = id;
    this.name = name;
    this.bonus = bonus;
    this.checked = checked;
  };

  // Data Structure / State
  const data = {
    items: [
      {
        id: 1,
        name: "Maths: Mathematical Methods (all)",
        bonus: 0,
        checked: false
      },
      {
        id: 2,
        name: "Maths: Specialist Mathematics",
        bonus: 0,
        checked: false
      },
      { id: 3, name: "Biology", bonus: 0, checked: false },
      { id: 4, name: "Physics", bonus: 0, checked: false },
      { id: 5, name: "Environmental Science", bonus: 0, checked: false },
      { id: 6, name: "Psychology", bonus: 0, checked: true },
      { id: 7, name: "Chemistry", bonus: 0, checked: false },
      { id: 8, name: "Geography", bonus: 0, checked: false }
    ],
    currentItem: null,
    totalBonus: 0,
    estimate: 0,
    totalAtar: 0
  };

  // Public methods
  return {
    getItems: function() {
      return data.items;
    },
    addItem: function(name, bonus) {
      let ID;
      // Create ID
      if (data.items.length > 0) {
        ID = data.items[data.items.length - 1].id + 1;
      } else {
        ID = 0;
      }
      bonus = parseInt(bonus);

      // Create new item
      newItem = new Item(ID, name, bonus);

      // Add to items array
      data.items.push(newItem);

      return newItem;
    },
    getItemById: function(id) {
      let found = null;
      // Loop through items
      data.items.forEach(function(item) {
        if (item.id === id) {
          found = item;
        }
      });
      return found;
    },
    updateItem: function(bonus) {
      bonus = parseInt(bonus);

      let found = null;

      data.items.forEach(function(item) {
        if (item.id === data.currentItem.id) {
          item.bonus = bonus;
          found = item;
        }
      });
      return found;
    },

    clearAllItems: function() {
      data.items = [];
    },
    clearTotal: function() {
      data.totalAtar = 0;
    },
    setCurrentItem: function(item) {
      data.currentItem = item;
    },
    setBonusScore: function(bonus) {
      data.bonus = bonus;
    },
    getCurrentItem: function() {
      return data.currentItem;
    },

    //get bonus
    getTotalBonus: function() {
      let total = 0;

      // Loop through items and add cals
      data.items.forEach(function(item) {
        total += item.bonus;
      });

      // Set total cal in data structure
      data.totalBonus = parseInt(total);
      // Return total
      return data.totalBonus;
    },
    getEstimate: function(input) {
      data.estimate = input;
      console.log("data estimate" + data.estimate);
      return data.estimate;
    },
    getAtar: function() {
      const bonus = parseInt(data.totalBonus);
      const estimate = parseInt(data.estimate);
      const total = bonus + estimate;
      return total;
    },
    logData: function() {
      return data;
    },

    checkScaleLookup: fullAtar => {
      for (let i = 0; i < ATAR_SCALED.length; i++) {
        if (ATAR_SCALED[i].low <= fullAtar && ATAR_SCALED[i].high >= fullAtar) {
          let scaledVal = ATAR_SCALED[i].atar;
          return scaledVal;
        }
      }
    },

    checkRawLookup: fullAtar => {
      for (let i = 0; i < ATAR_SCALED.length; i++) {
        if (Number(ATAR_SCALED[i].atar) == Number(fullAtar)) {
          const scaledVal = ATAR_SCALED[i].low;
          return parseFloat(scaledVal);
        }
      }
    }
  };
})();

// UI Controller
const UICtrl = (function() {
  const UISelectors = {
    itemList: "#item-list",
    listItems: "#item-list li",
    addBtn: ".add-btn",
    updateBtn: ".update-btn",
    deleteBtn: ".delete-btn",
    backBtn: ".back-btn",
    clearBtn: ".clear-btn",
    itemNameInput: "#item-name",
    totalScaledAtar: ".total-atar-score",
    totalBonus: ".total-bonus-score",
    estimateATAR: ".estimate-atar",
    showCourses: ".view-courses"
  };

  // Public methods
  return {
    populateItemList: function(items) {
      let html = "";

      items.forEach(function(item) {
        html += `
        <div class="row">
          <label class="collection-item sq-form-question-title col-12" id="item-${
            item.id
          }">
          <div class="sq-form-question-answer row">
            <div class="col-md-8 col-9 select-control">${item.name}</div> 
            <input type="textarea" class="bonus-subject sq-form-field col-md-1 col-3" minlength="2" maxlength="2" size="4" value="${
              item.bonus
            }">
              </input>
          </div>
          </label>
        </div>`;
      });

      // Insert list items
      document.querySelector(UISelectors.itemList).innerHTML = html;
    },
    getItemInput: function() {
      return {
        name: document.querySelector(UISelectors.itemNameInput).value,
        bonus: document.querySelector(UISelectors.itemCaloriesInput).value,
        estimate: document.querySelector(UISelectors.estimateATAR).value
      };
    },

    checkInputField: function() {
      document.querySelector("course-checkbox").checked = true;
    },

    // total bonus
    showTotalBonus: function(totalBonus) {
      document.querySelector(UISelectors.totalBonus).textContent = totalBonus;
    },
    //ATAR estimate manual input
    showEstimateAtar: function(estimateATAR) {
      document.querySelector(UISelectors.estimateATAR).value = estimateATAR;
    },

    // Total ATAR
    showTotalAtar: function(totalAtar) {
      document.querySelector(
        UISelectors.totalScaledAtar
      ).textContent = totalAtar;
    },

    showCourses: function(totalAtar) {
      document
        .querySelector(UISelectors.showCourses)
        .setAttribute(
          "href",
          `https://www.monash.edu/study/courses/find-a-course/2019/science-s2000?domestic=true`
        );
    },

    getSelectors: function() {
      return UISelectors;
    }
  };
})();
// App Controller
const App = (function(ItemCtrl, StorageCtrl, UICtrl) {
  // Load event listeners
  const loadEventListeners = function() {
    const estimate = document.querySelector(".estimate-atar");
    const inputs = document.getElementById("item-list");

    // Calculate total ATAR
    estimate.addEventListener("change", calcAtar);

    // Calculate bonus points
    inputs.addEventListener("change", calcBonus);

    // Get UI selectors
    const UISelectors = UICtrl.getSelectors();
  };

  const calcBonus = () => {
    let total = 0;
    function count(obj) {
      return Object.keys(obj).length;
    }

    function countTwoPoints(num) {
      let total = 0;
      if (num > 1) {
        total = num * 2;
      } else {
        total = 0;
      }
      return total;
    }

    function countThreePoints(num) {
      let total = 0;
      if (num > 1) {
        total = num * 3;
      } else {
        total = 0;
      }
      return total;
    }

    function validateValue(sum) {
      if (sum >= 8) {
        return 8;
      } else {
        return 0;
      }
    }

    const kvpairs = [];
    let twoPointCount = 0;
    const inputFields = document.getElementsByClassName("bonus-subject");

    for (var i = 0; i < inputFields.length; i++) {
      var e = inputFields[i].value;
      kvpairs.push(e);
    }
    const twoPoints = kvpairs.filter(x => x >= 25 && x < 30);
    let count2 = count(twoPoints);

    const threePoints = kvpairs.filter(x => x >= 30 && x <= 50);
    const count3 = count(threePoints);

    console.log("3 points " + count3);
    console.log("2 points " + count2);
    //2 x subject study score 25 0 x subject study score 30 +
    if (count2 == 2 && count3 == 0) {
      console.log("4 points");
      total = 4;
      // 1 x subject study score 30 + 2 x subjects study score 25
    } else if (count2 == 3 && count3 == 0) {
      console.log("6 points");
      total = 6;
      // 4 x subjects study score 25
    } else if (count2 == 4 && count3 == 0) {
      console.log("8 points");
      total = 8;
      // 1 x subject study score 30 + 2 x subjects study score 25
    } else if (count2 == 1 && count3 == 1) {
      console.log("4 points");
      total = 4;
      // 1 x subject study score 30 + 2 x subjects study score 25
    } else if (count2 == 2 && count3 == 1) {
      total = 6;
      // 1 x subject study score 30 + 3 x subjects study score 25
    } else if (count2 == 3 && count3 == 1) {
      total = 8;

      // 2 x subjects study score 30 + 1 x subject study score 25
    } else if (count2 == 1 && count3 == 2) {
      total = 6;
      //   2 x subjects study score 30 + 2 x subject study score 25
    } else if (count2 == 0 && count3 == 2) {
      total = 6;
      //   2 x subjects study score 30 + 2 x subject study score 25
    } else if (count2 == 2 && count3 == 2) {
      total = 8;
    } else {
      const sum = parseInt(countTwoPoints(count2) + countThreePoints(count3));
      const validateTotal = validateValue(sum);
      total = validateTotal;
    }

    console.log(kvpairs);
    console.log(total);

    UICtrl.showTotalBonus(total);
  };

  const calcAtar = () => {
    let input = document.querySelector(".estimate-atar").value;
    const rawInput = ItemCtrl.checkRawLookup(input);
    let bonus = document.getElementsByClassName("total-bonus-score")[0]
      .textContent;

    let bonusInt = parseInt(bonus);
    let rawInputIn = parseFloat(rawInput);

    let totalAtar = parseFloat(rawInputIn + bonusInt);
    let scaledAtar = ItemCtrl.checkScaleLookup(totalAtar);
    const scaleAtarInt = parseFloat(scaledAtar);

    UICtrl.showTotalAtar(scaleAtarInt);

    UICtrl.showCourses(scaledAtar);
  };

  // Public methods
  return {
    init: function() {
      // Fetch items from data structure
      const items = ItemCtrl.getItems();

      // Populate list with items
      UICtrl.populateItemList(items);

      // Load event listeners
      loadEventListeners();
    }
  };
})(ItemCtrl, StorageCtrl, UICtrl);

// Initialize App
App.init();
